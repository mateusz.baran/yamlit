import warnings
from typing import Any, Dict


class YamlItRegister(dict):
    def __init__(self, *args, **kwargs) -> None:
        super(YamlItRegister, self).__init__(*args, **kwargs)
        self._inverse = {v: k for k, v in self.items()}

    @property
    def inverse(self) -> Dict[Any, str]:
        return self._inverse.copy()

    def __hash__(self) -> int:  # type: ignore
        return id(self)

    def __setitem__(self, key: str, value: Any) -> None:
        if '.' in key:
            raise KeyError(f"Key {key} cannot contain a period")

        if key in self:
            warnings.warn(f"Removing duplicated value {self[key]} for key {key}")
            self._inverse.pop(self[key])

        if isinstance(value, YamlItRegister):
            common_values = set(self._inverse).intersection(value._inverse)
            if common_values:
                warnings.warn(f"Overriding inverse value of {', '.join(common_values)}")
            self._inverse.update()
        else:
            if value in self.inverse:
                warnings.warn(f"Overriding inverse value of {self.inverse[value]}")
            self._inverse[value] = key

        super(YamlItRegister, self).__setitem__(key, value)

    def __delitem__(self, key: str) -> None:
        self._inverse.pop(self[key])
        super(YamlItRegister, self).__delitem__(key)

    def __getitem__(self, key: str) -> Any:
        key, _, sub_key = str(key).strip('.').partition('.')
        obj = super().__getitem__(key)
        if sub_key:
            return obj[sub_key]
        return obj
