from __future__ import annotations

import functools
import inspect
import typing
from abc import ABCMeta, abstractmethod
from typing import Any, Callable, Dict, Iterator, List, Optional, Tuple, Union

if typing.TYPE_CHECKING:
    from yamlit.config import YamlItConfig


class YamlItType(metaclass=ABCMeta):
    @abstractmethod
    def eval(self) -> Any:
        raise NotImplementedError


class YamlItMapping(YamlItType, dict):
    def __init__(self, **kwargs) -> None:
        super(YamlItMapping, self).__init__(**kwargs)

    def eval(self) -> Dict[str, Any]:
        return {k: v.eval() if isinstance(v, YamlItType) else v for k, v in self.items()}

    def __getitem__(self, key: str) -> Any:
        key, _, sub_key = str(key).strip('.').partition('.')

        if not key:
            raise KeyError("Key cannot be empty")

        obj = super().__getitem__(key)

        if sub_key:
            return obj[sub_key]

        if isinstance(obj, YamlItScalar):
            return obj.eval()

        return obj

    def __setitem__(self, key: str, value: Any) -> None:
        if '.' in key:
            raise KeyError(f"Key {key} cannot contain a period")

        super().__setitem__(key, value)

    def __iter__(self) -> Iterator[Any]:
        yield from super(YamlItMapping, self).__iter__()


class YamlItSequence(YamlItType, list):
    def __init__(self, *args) -> None:
        super(YamlItSequence, self).__init__(args)

    def eval(self) -> List[Any]:
        return [v.eval() if isinstance(v, YamlItType) else v for v in self]

    def __getitem__(self, item: Union[int, str]) -> Any:  # type: ignore
        key, _, sub_key = str(item).strip('.').partition('.')

        if not key:
            raise KeyError("Key cannot be empty")

        obj = super(YamlItSequence, self).__getitem__(int(key))

        if sub_key:
            return obj[sub_key]

        if isinstance(obj, YamlItScalar):
            return obj.eval()

        return obj

    def __iter__(self) -> Iterator[Any]:
        yield from (self[i] for i in range(self.__len__()))


class YamlItScalar(YamlItType, metaclass=ABCMeta):
    pass


class YamlItLazyObject(YamlItScalar):
    NAME = '__name__'
    POSITIONAL_ARGUMENT_KEY = '__pos__'
    PARTIAL = '__partial__'

    def __init__(
        self,
        config: YamlItConfig,
        obj: Callable,
        args: Tuple[Any, ...] = tuple(),
        kwargs: Optional[Dict[str, Any]] = None,
        instantiate: bool = True,
        partial: bool = False,
    ) -> None:
        self._config = config
        self._obj = obj
        self._args = args
        self._kwargs = kwargs or dict()
        self._instantiate = instantiate
        self._partial = partial

        self._return: Any = None
        self._evaluated: bool = False

    def eval(self) -> Any:
        if not self._evaluated:
            if self._instantiate:
                args = YamlItSequence(*self._args)
                kwargs = YamlItMapping(**self._kwargs)

                if self._partial:
                    self._return = functools.partial(self._obj, *args, **kwargs)
                else:
                    self._return = self._obj(*args, **kwargs)
            else:
                self._return = self._obj
            self._evaluated = True
        return self._return

    def to_dict(self, only_args: bool = False) -> Dict[str, Any]:
        representation: Dict[str, Any] = dict()
        if not only_args:
            representation[self.NAME] = self._config.register.inverse[self._obj]

        is_class = inspect.isclass(self._obj)
        obj = getattr(self._obj, '__init__') if is_class else self._obj

        signature = inspect.signature(obj)
        type_hints = typing.get_type_hints(obj)

        params = iter(signature.parameters.values())
        if is_class:
            next(params)

        for param in params:
            default: typing.Any

            annotation = param.annotation
            if param.name in type_hints:
                annotation = type_hints[param.name]

            if annotation in self._config.register.inverse:
                default = YamlItLazyObject(self._config, obj=annotation)
            elif param.default is inspect.Parameter.empty:
                default = '?'
            else:
                default = param.default

            if param.kind == inspect.Parameter.POSITIONAL_ONLY:
                if self.POSITIONAL_ARGUMENT_KEY not in representation:
                    representation[self.POSITIONAL_ARGUMENT_KEY] = list()
                representation[self.POSITIONAL_ARGUMENT_KEY].append(default)
            else:
                representation[param.name] = default

        return representation

    def __str__(self) -> str:
        return str(self._obj)

    def __repr__(self) -> str:
        return repr(self._obj)


class YamlItDependency(YamlItScalar):
    def __init__(
        self,
        config: YamlItConfig,
        reference: str,
        attribute: str,
        is_callable: bool,
    ) -> None:
        super(YamlItDependency, self).__init__()
        self._config = config
        self._reference = reference
        self._attribute = attribute
        self._is_callable = is_callable

    @property
    def object(self) -> Any:
        return self._config.load()[self._reference]

    def eval(self) -> Any:
        if self._attribute:
            if self._is_callable:
                return getattr(self.object, self._attribute)()
            return getattr(self.object, self._attribute)
        return self.object

    def __str__(self) -> str:
        attr = f":{self._attribute}" if self._attribute else ""
        call = "()" if self._is_callable else ""
        return self._reference + attr + call

    def __repr__(self) -> str:
        return str(self)
