from __future__ import annotations

import os
import re
import typing
from pathlib import Path
from typing import BinaryIO, Iterable, TextIO, Union

from yaml import Loader, MappingNode, Node, SafeLoader, ScalarNode

from yamlit.structures import YamlItDependency, YamlItLazyObject, YamlItMapping, YamlItSequence

if typing.TYPE_CHECKING:
    from yamlit.config import YamlItConfig


class YamlItLoader(SafeLoader):
    ENV_SEPARATOR = ':'
    ENV_PATTERN = re.compile(r'.*?\${(.*?)}.*?')

    def __init__(self, config: YamlItConfig, stream: Union[BinaryIO, TextIO]) -> None:
        super(YamlItLoader, self).__init__(stream)
        self._config = config

        self.yaml_constructors = self.yaml_constructors.copy()
        self.yaml_constructors['!deps'] = self.construct_yamlit_dependency
        self.yaml_constructors['!obj'] = self.construct_yamlit_lazy_object

    def construct_yaml_seq(self, node: Node) -> Iterable[YamlItSequence]:
        data = YamlItSequence()
        yield data
        data.extend(self.construct_sequence(node))

    def construct_yaml_map(self, node: Node) -> Iterable[YamlItMapping]:
        data = YamlItMapping()
        yield data
        value = self.construct_mapping(node)
        data.update(value)

    def construct_yamlit_dependency(self, loader: Loader, node: Node) -> YamlItDependency:
        value = loader.construct_yaml_str(node)
        value, _, attribute = value.partition(':')
        is_callable = attribute.endswith('()')

        if is_callable:
            attribute = attribute[:-2]

        return YamlItDependency(
            config=self._config,
            reference=value,
            attribute=attribute,
            is_callable=is_callable,
        )

    def construct_yamlit_lazy_object(self, loader: Loader, node: Node) -> YamlItLazyObject:
        if isinstance(node, ScalarNode):
            name = loader.construct_scalar(node)
            return YamlItLazyObject(
                config=self._config,
                obj=self._config.register[str(name)],
                instantiate=False,
            )

        if isinstance(node, MappingNode):
            kwargs = loader.construct_mapping(node)
            args = kwargs.pop(YamlItLazyObject.POSITIONAL_ARGUMENT_KEY, tuple())

            name = kwargs.pop(YamlItLazyObject.NAME)
            partial = kwargs.pop(YamlItLazyObject.PARTIAL, False)

            return YamlItLazyObject(
                config=self._config,
                obj=self._config.register[str(name)],
                args=args,
                kwargs=kwargs,
                partial=partial,
            )

        raise ValueError(f"Node {node} is not supported")

    def construct_env_var(self, node: Node) -> Path:
        value = self.construct_yaml_str(node)
        for group in self.ENV_PATTERN.findall(value):
            key, _, default = str(group).partition(self.ENV_SEPARATOR)
            value = value.replace(f"${{{group}}}", os.environ.get(key, default=default))
        return value

    def construct_path(self, node: Node) -> Path:
        value = self.construct_yaml_str(node)
        return Path(value)


YamlItLoader.add_constructor(tag='!env',
                             constructor=YamlItLoader.construct_env_var)
YamlItLoader.add_constructor(tag='!path',
                             constructor=YamlItLoader.construct_path)
YamlItLoader.add_constructor(tag='tag:yaml.org,2002:seq',
                             constructor=YamlItLoader.construct_yaml_seq)
YamlItLoader.add_constructor(tag='tag:yaml.org,2002:map',
                             constructor=YamlItLoader.construct_yaml_map)
