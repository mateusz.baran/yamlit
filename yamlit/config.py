from __future__ import annotations

import argparse
from pathlib import Path
from typing import Any, Callable, Dict, Optional

import yaml

from yamlit.dumper import YamlItDumper
from yamlit.loader import YamlItLoader
from yamlit.register import YamlItRegister
from yamlit.structures import YamlItLazyObject, YamlItMapping


class YamlItConfig:
    TAG_NAME = '__main__'

    def __init__(
        self,
        register: Optional[YamlItRegister] = None,
        path: Optional[Path] = None,
        squash: bool = True,
    ) -> None:
        self._path = path
        self._register = register or YamlItRegister()
        self._squash = squash

        self._config: Optional[YamlItMapping] = None
        self._sections: Dict[str, Any] = dict()

    @property
    def register(self) -> YamlItRegister:
        return self._register

    def add_section(self, name: str, obj: Callable) -> YamlItConfig:
        self._sections[name] = obj
        self._register[name] = obj
        return self

    def load(self) -> YamlItMapping:
        if self._config is not None:
            return self._config

        if self._path is None:
            raise ValueError("Path is not provided")

        with self._path.open() as stream:
            loader = YamlItLoader(self, stream)

        try:
            config = loader.get_single_data()
        finally:
            loader.dispose()

        if not isinstance(config, YamlItMapping):
            raise RuntimeError(f"Config {self._path} is not a dict")

        self._config = config
        return self._config

    def dump(self) -> None:
        if self._path is None:
            raise ValueError("Path is not provided")

        if not self._sections:
            raise ValueError("Sections are not added")
        else:
            config = dict()
            for name, obj in self._sections.items():
                config[name] = YamlItLazyObject(config=self, obj=obj)

        if self._squash and len(self._sections) == 1:
            config = config[self.TAG_NAME].to_dict(only_args=True)

        with self._path.open('w') as file:
            yaml.dump(config, file, Dumper=YamlItDumper)

    def wrap(self, func: Callable) -> Callable:
        self.add_section(name=self.TAG_NAME, obj=func)
        return func

    def run(self) -> Any:
        parser = argparse.ArgumentParser()
        parser.add_argument('--path', type=Path, required=self._path is None or self._path.is_dir())
        parser.add_argument('--dump', default=False, action='store_true')
        args = parser.parse_args()

        if args.path:
            if self._path is None or not self._path.is_dir():
                self._path = args.path
            else:
                self._path = self._path.joinpath(args.path)

        if args.dump:
            self.dump()
        else:
            kwargs = self.load()
            args = kwargs.pop(YamlItLazyObject.POSITIONAL_ARGUMENT_KEY, tuple())
            func = self._sections[self.TAG_NAME]
            return func(*args, **kwargs)
