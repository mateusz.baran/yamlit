from pathlib import Path

from yaml import Node, SafeDumper

from yamlit.structures import YamlItDependency, YamlItLazyObject, YamlItMapping, YamlItSequence


class YamlItDumper(SafeDumper):
    def represent_path(self, data: Path) -> Node:
        return self.represent_str(str(data))

    def represent_yamlit_dependency(self, data: YamlItDependency) -> Node:
        node = self.represent_data(str(data))
        node.tag = '!deps'
        return node

    def represent_yamlit_lazy_object(self, data: YamlItLazyObject) -> Node:
        node = self.represent_dict(data.to_dict())
        node.tag = '!obj'
        return node


YamlItDumper.add_multi_representer(data_type=Path,
                                   representer=YamlItDumper.represent_path)
YamlItDumper.add_representer(data_type=YamlItMapping,
                             representer=YamlItDumper.represent_dict)
YamlItDumper.add_representer(data_type=YamlItSequence,
                             representer=YamlItDumper.represent_list)
YamlItDumper.add_representer(data_type=YamlItLazyObject,
                             representer=YamlItDumper.represent_yamlit_lazy_object)
YamlItDumper.add_representer(data_type=YamlItDependency,
                             representer=YamlItDumper.represent_yamlit_dependency)
